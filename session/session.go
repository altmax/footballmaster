package session

import (
	"crypto/rand"
	"errors"
	"fmt"
	"strconv"

	"bitbucket.org/altmax/masterServer/relayServer"
)

func GenerateId() string {
	b := make([]byte, 16)
	rand.Read(b)
	return fmt.Sprintf("%x", b)
}

type Room struct {
	token    string
	flag     bool
	firstID  string
	secondID string
	relay    *relayServer.Relay
}

type Session struct {
	Rooms map[string]*Room
}

func NewSession() *Session {
	s := new(Session)
	s.Rooms = make(map[string]*Room)
	return s
}

func (s *Session) CreateRoom(firstID string) *Room {
	token := GenerateId()
	room := &Room{firstID: firstID, flag: false, token: token}
	s.Rooms[token] = room
	return room
}

func (s *Session) FindRoom(ID string) *Room {
	for _, v := range s.Rooms {
		if !v.flag {
			err := v.SetUser(ID)
			if err == nil {
				return v
			}
		}
	}
	room := s.CreateRoom(ID)
	return room
}

func (s *Session) GetRoomToken(ID string) string {
	r := s.GetRoomByUserID(ID)
	if r != nil {
		return r.Token()
	}
	return ""
}

func (s *Session) GetAll() {
	i := 1
	for _, v := range s.Rooms {
		var quant int
		if v.flag {
			quant = 2
		} else {
			quant = 1
		}
		fmt.Println(strconv.Itoa(i) + ") quant = " + strconv.Itoa(quant) + " token = " + v.token)
		i++
	}
}

func (s *Session) GetRoomByUserID(ID string) *Room {
	for _, v := range s.Rooms {
		if v.GetUserID(1) == ID || v.GetUserID(2) == ID {
			return v
		}
	}
	return nil
}

func (s *Session) GetRoomByToken(token string) *Room {
	r, ok := s.Rooms[token]
	if ok {
		return r
	}
	return nil
}

func (r *Room) SetRelay(relay *relayServer.Relay) {
	r.relay = relay
}

func (r *Room) SetUser(ID string) error {
	if r.flag {
		return errors.New("full")
	}
	if r.firstID == "" {
		r.firstID = ID
		return nil
	}
	r.secondID = ID
	r.flag = true
	return nil
}

func (r Room) GetUserID(num int) string {
	if num == 1 {
		return r.firstID
	}
	if num == 2 {
		return r.secondID
	}
	return ""
}

func (r Room) GetStatus() int {
	if r.flag {
		return 2
	}
	return 1
}

func (r Room) Token() string {
	return r.token
}

func (r Room) Relay() *relayServer.Relay {
	return r.relay
}
