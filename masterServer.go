package main

import (
	"log"

	"net/http"

	"io"

	"strconv"

	"encoding/json"

	"bitbucket.org/altmax/masterServer/relayServer"
	"bitbucket.org/altmax/masterServer/session"
)

// const localAddr = "172.16.20.213"
const startRelayPort = 10002
const masterPort = 10001
const maxRelayConnect = 2
const httpPort = 10000

var (
	inMemorySession *session.Session
	// connectList     []string
	// relayList     []*relayServer.Relay
	nextRelayPort = startRelayPort
	// localAddr     string
)

func main() {
	// go footballHTTP.ServerInit(httpPort)
	// go relayServer.Start()
	// startRelayPort := ":10002"
	// nextRelayPort++
	// relayList = append(relayList, Relay{Addr: relayAddr + ":" + strconv.Itoa(nextRelayPort), Count: 0})
	// go relayServer.Start(nextRelayPort)
	// NextRelayServerStart(false)
	inMemorySession = session.NewSession()
	// b, err := ioutil.ReadFile("config.json")
	// if err != nil {
	// 	log.Println("config.json not found")
	// 	return
	// }
	// type Config struct {
	// 	LocalAddr string `json:localAddr`
	// }
	// var c Config
	// _ = json.Unmarshal(b, &c)
	// localAddr = c.LocalAddr
	// ServerAddr, err := net.ResolveUDPAddr("udp", ":"+strconv.Itoa(masterPort))
	// if err != nil {
	// 	log.Panic(err)
	// }
	// l, err := net.ListenUDP("udp", ServerAddr)
	// defer l.Close()
	// buf := make([]byte, 1024)
	// for {
	// 	n, addr, err := l.ReadFromUDP(buf)
	// 	msg := string(buf[0:n])
	// 	log.Println("from ", addr.String(), " ", msg)

	// 	if err != nil {
	// 		log.Panic(err)
	// 	}

	// 	if addr.String() != localAddr+":"+strconv.Itoa(httpPort) {
	// 		continue
	// 	}

	// 	if msg == "matchmaking" {
	// 		go Connect(l, addr)
	// 	}
	// }
	h := http.NewServeMux()
	h.HandleFunc("/findRoom", FindRoom)
	h.HandleFunc("/getRelayPort", GetRelayServerPort)
	// midd := CheckAddr(h)
	log.Println("Listen 10001")
	http.ListenAndServe(":10001", h)
}

func CheckAddr(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Println(r.Body)
		r.ParseForm()
		go next.ServeHTTP(w, r)
	})
}

func FindRoom(w http.ResponseWriter, r *http.Request) {
	log.Println("/findRoom")
	id := r.FormValue("id")
	log.Println("ID : ", id)
	room := inMemorySession.GetRoomByUserID(id)
	var token string
	if room == nil {
		room = inMemorySession.FindRoom(id)
	}
	token = room.Token()
	log.Println(token)
	str, _ := json.Marshal(map[string]string{"token": token})
	io.WriteString(
		w,
		string(str),
	)
}

func GetRelayServerPort(w http.ResponseWriter, r *http.Request) {
	id := r.FormValue("id")
	room := inMemorySession.GetRoomByUserID(id)
	if room == nil {
		return
	}
	var res string
	relay := room.Relay()
	if relay == nil {
		relay = NextRelayServerStart(room.Token())
		room.SetRelay(relay)
		// res = relay.Port()
	}
	res = strconv.Itoa(relay.Port())
	str, _ := json.Marshal(map[string]string{"port": res})
	io.WriteString(w, string(str))
}

func NextRelayServerStart(token string) *relayServer.Relay {
	nextRelayPort++
	relay := relayServer.NewRelay(nextRelayPort, token)
	// relayList = append(relayList, relay)
	go relay.Start()
	log.Println("Start new relayServer on :", nextRelayPort)
	return relay
}

// func Connect(l *net.UDPConn, addr *net.UDPAddr) {
// 	log.Println("Connect from " + fmt.Sprint(addr))
// 	find := false
// 	addrStr := addr.String()
// loop:
// 	for _, v := range connectList {
// 		if v == addrStr {
// 			find = true
// 			break loop
// 		}
// 	}
// 	if !find {
// 		connectList = append(connectList, addrStr)

// 		token := inMemorySession.FindRoom(addr)
// 		log.Println("For addr " + fmt.Sprint(addr) + " token = " + token)
// 		l.WriteToUDP([]byte(token), addr)
// 		if inMemorySession.GetStatus(addr) == 2 {
// 			addrs := inMemorySession.GetRoomAddrs(addr)
// 			relay := GetFreeRelayAddr()
// 			for _, v := range addrs {
// 				l.WriteToUDP([]byte("success::"+relay), v)
// 			}

// 		}
// 	} else {
// 		flag := inMemorySession.GetStatus(addr)
// 		var res string
// 		switch flag {
// 		case 2:
// 			res = "success"
// 		case 1:
// 			res = "wait"
// 		case -1:
// 			res = "error"
// 		}
// 		l.WriteToUDP([]byte(res), addr)
// 	}
// 	inMemorySession.GetAll()
// }
