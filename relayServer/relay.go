package relayServer

import (
	"encoding/xml"
	"fmt"
	"log"
	"net"
	"strconv"
)

type Relay struct {
	port  int
	addr  string
	token string
	pair  *Pair
	run   bool
}

func NewRelay(port int, token string) *Relay {
	r := Relay{port: port, token: token, pair: &Pair{}}
	return &r
}

func (r *Relay) IsRun() bool {
	return r.run
}

func (r *Relay) Port() int {
	return r.port
}

func (r *Relay) Start() {
	log.Println("relay start")
	r.run = true
	strPort := ":" + strconv.Itoa(r.port)
	ServerAddr, err := net.ResolveUDPAddr("udp", strPort)
	if err != nil {
		log.Panic(err)
	}
	l, err := net.ListenUDP("udp", ServerAddr)
	defer l.Close()
	buf := make([]byte, 1024)
loop:
	for r.run {
		n, addr, err := l.ReadFromUDP(buf)
		msg := string(buf[0:n])
		fmt.Println("from ", addr.String(), " ", msg)
		// addrStr := addr.String()
		if err != nil {
			log.Panic(err)
		}
		// l.WriteToUDP([]byte("test"), addr)
		if !r.checkAddr(addr) {
			if !r.checkToken(msg) {
				SendMsgToUDP(l, []byte("invalid token"), addr)
				continue loop
			}
			if !r.addPlayer(addr) {
				SendMsgToUDP(l, []byte("pair is full"), addr)
				continue loop
			} else {
				SendMsgToUDP(l, []byte("ok"), addr)
				continue loop
			}
		}
		if r.Pair().GetSecondAddr(addr) == nil {
			SendMsgToUDP(l, []byte("wait"), addr)
			continue loop
		}
		type Data struct {
			BX string `xml:"ball>x,attr"`
			BY string `xml:"ball>y,attr"`
			PX string `xml:"player>x,attr"`
			PY string `xml:"player>y,attr"`
		}
		var d Data
		err = xml.Unmarshal(buf[0:n], &d)
		if err == nil {
			SendMsgToUDP(l, []byte("invalid xml"), addr)
			continue loop
		}
		pair := r.Pair()
		if d.BX != "" {
			bx, _ := strconv.ParseFloat(d.BX, 32)
			by, _ := strconv.ParseFloat(d.BY, 32)
			ball := pair.Ball()
			ball.Coords().SetX(bx)
			ball.Coords().SetY(by)
		}
		if d.PX != "" {
			px, _ := strconv.ParseFloat(d.PX, 32)
			py, _ := strconv.ParseFloat(d.PY, 32)
			player := pair.GetPlayerByAddr(addr)
			if player != nil {
				player.Coords().SetX(px)
				player.Coords().SetY(py)
			}
		}
		SendMsgToUDP(l, buf[0:n], pair.GetSecondAddr(addr))
		log.Println(string(buf[0:n]))
		log.Println(d)
	}

}

func (r *Relay) Stop() {
	r.run = false
}

func (r *Relay) checkAddr(addr *net.UDPAddr) bool {
	return r.Pair().CheckAddr(addr)
}

func (r *Relay) checkToken(token string) bool {
	if token == r.token {
		return true
	}
	return false
}

func (r *Relay) addPlayer(addr *net.UDPAddr) bool {
	if r.pair.IsStartRecieve() {
		return false
	}
	return r.pair.AddPlayer(addr)
}

func (r *Relay) Pair() *Pair {
	return r.pair
}

func SendMsgToUDP(l *net.UDPConn, msg []byte, addr *net.UDPAddr) {
	log.Println("Relay:> send ", string(msg), " to ", addr.String())
	l.WriteToUDP(msg, addr)
}
