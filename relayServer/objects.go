package relayServer

import "net"

type Player struct {
	addr   *net.UDPAddr
	coords *Coords
}
type Ball struct {
	coords *Coords
}

type Coords struct {
	X float32
	Y float32
}

func (p *Player) Addr() *net.UDPAddr {
	return p.addr
}

func (p *Player) Init(addr *net.UDPAddr) {
	p.coords = &Coords{0, 0}
	p.addr = addr
}

func (p *Player) SetAddr(addr *net.UDPAddr) {
	p.addr = addr
}

func (p *Player) Coords() *Coords {
	return p.coords
}

func (b *Ball) Coords() *Coords {
	return b.coords
}

func (c *Coords) SetX(x float64) {
	c.X = float32(x)
}

func (c *Coords) SetY(y float64) {
	c.Y = float32(y)
}
