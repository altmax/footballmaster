package relayServer

import "net"

type Pair struct {
	startRecieve bool
	first        *Player
	second       *Player
	ball         *Ball
	count        int
}

func (p *Pair) IsStartRecieve() bool {
	return p.isFull()
}

func (p *Pair) GetSecondAddr(addr *net.UDPAddr) *net.UDPAddr {
	if p.Player(1).Addr().String() == addr.String() {
		return p.Player(2).Addr()
	}
	return p.Player(1).Addr()
}

func (p *Pair) AddPlayer(addr *net.UDPAddr) bool {
	if p.isFull() {
		return false
	}
	p.getFreePlayer().Init(addr)
	p.count++
	return true
}

func (p Pair) isFull() bool {
	if p.count == 2 {
		return true
	}
	return false
}

func (p *Pair) Player(num int) *Player {
	if num == 1 {
		if p.first == nil {
			p.first = &Player{}
		}
		return p.first
	}
	if p.second == nil {
		p.second = &Player{}
	}
	return p.second
}

func (p *Pair) Ball() *Ball {
	return p.ball
}

func (p *Pair) StartRecieve() {
	if !p.startRecieve {
		p.startRecieve = true
	}
}

func (p *Pair) CheckAddr(addr *net.UDPAddr) bool {
	// if p.Player(1) != nil {
	if p.Player(1).Addr().String() == addr.String() {
		return true
	}
	// }
	// if p.Player(2) != nil {
	if p.Player(2).Addr().String() == addr.String() {
		return true
	}
	// }
	return false
}

func (p *Pair) getFreePlayer() *Player {
	if p.count == 0 {
		return p.Player(1)
	}
	if p.count == 1 {
		return p.Player(2)
	}
	return nil
}

func (p *Pair) GetPlayerByAddr(addr *net.UDPAddr) *Player {
	if addr.String() == p.Player(1).Addr().String() {
		return p.Player(1)
	}
	if addr.String() == p.Player(2).Addr().String() {
		return p.Player(2)
	}
	return nil
}
